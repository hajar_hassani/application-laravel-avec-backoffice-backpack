<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 ---------------------------------------------------------
  ROUTES AJAX
 ---------------------------------------------------------
*/

/*
  AJAX CHARGEMENT DES OLDER PROJETS
  PATERN: /ajax/older-works
  CTRL: ProjetsController
  ACTION: ajaxOlders
*/
Route::get('/ajax/older-works', 'ProjetsController@ajaxOlders')->name('projets.ajax.olders');


/*
 ---------------------------------------------------------
  ROUTES STANDARS
 ---------------------------------------------------------
*/

/*
  ROUTE PAR DEFAUT
  PATERN: /
  CTRL: Pages
  ACTION: index
*/
Route::get('/', 'PagesController@show')->defaults('page', 1)->name('app');

/*
  ROUTE DETAILS D'UNE PAGE
  PATERN: /pages/id/slug.html
  CTRL: Pages
  ACTION: show
*/
Route::get('/pages/{page}/{slug}.html', 'PagesController@show')
      ->where([
          'pages' => '[1-9][0-9]*',
          'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
      ])
      ->name('pages.show');

/*
  ROUTE DES POSTS
*/
Route::prefix('posts')->name('posts.')->group(function (){
  /*
    ROUTE DETAILS D'UN POST
    PATERN: /posts/id/slug.html
    CTRL: PostsController
    ACTION: show
  */
  Route::get('/{post}/{slug}.html', 'PostsController@show')
        ->where([
              'post' => '[1-9][0-9]*',
              'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
            ])
        ->name('show');
});

/*
  ROUTE DES PROJETS
*/
Route::prefix('projets')->name('projets.')->group(function (){
  /*
    ROUTE DETAILS D'UN PROJET
    PATERN: /projet/id/slug.html
    CTRL: ProjetsController
    ACTION: show
  */
  Route::get('/{projet}/{slug}.html', 'ProjetsController@show')
        ->where([
              'projet' => '[1-9][0-9]*',
              'slug' => '[a-z0-9][a-z0-9\-]*[a-z0-9]'
            ])
        ->name('show');
});

/*
  ROUTE DETAILS D'UNE CATEGORIE
  PATTERN: /categories/id/slug.html
  CTRL: Categories
  ACTION: show
 */
  Route::get('/categories/{categorie}/{slug}.html','CategoriesController@show')
        ->where([
             'categorie' => '[1-9][0-9]*',
             'slug' => '[a-z0-9][a-z0-9\-]*'
        ])
        ->name('categories.show');
