{{--
  ./resources/views/categories/show.blade.php
  variables disponibles :
      - $categories $categorie
      - $posts ?? '' $post

 --}}

 @extends('template.app')

 @section('titre')
   {{$categorie->nom}}
 @endsection

 @section('content1')
   <!-- Page Title -->
   <div class="section section-breadcrumbs">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>Categorie {{ $categorie->nom }}</h1>
         </div>
       </div>
     </div>
   </div>

   <!-- Les posts -->
   @include('posts.index')

 @endsection
