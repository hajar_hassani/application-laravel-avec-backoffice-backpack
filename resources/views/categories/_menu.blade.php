{{--
  ./resources/views/categories/menu.blade.php
  variables disponibles :
      - $categories Array(Categorie)
 --}}

   <ul class="blog-categories">
      @foreach ($categories as $categorie)
     <li>
       <a href="{{ URL::route('categories.show', [
              'categorie'  => $categorie->id,
              'slug' => Str::slug($categorie->nom, '-')
              ]) }}">
         {{ $categorie->nom }}
       </a>
     </li>
     @endforeach
   </ul>
