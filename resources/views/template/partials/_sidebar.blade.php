{{--
  ./resources/views/template/partials/_sidebar.blade.php
  Sidebar
 --}}

 <hr>
<!-- Our Articles -->
<div class="section">
  <div class="container">
    <div class="row">
      <!-- Featured News -->
      <div class="col-sm-6 featured-news">
        @include(
          'posts.latest',
          ['posts' => \App\Models\Post::all()->take(3)]
        )
      </div>
      <!-- End Featured News -->


      <!-- Latest News FB -->
      <div class="col-sm-6 latest-news">
        <h2>Lastest Twitter News</h2>
        <div class="row">
          <div class="col-sm-12">
            <a class="twitter-timeline" href="https://twitter.com/HajarHassani_?ref_src=twsrc%5Etfw">Tweets by HajarHassani_</a>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
        </div>
      </div>
      <!-- End Featured News -->
    </div>
  </div>
</div>
