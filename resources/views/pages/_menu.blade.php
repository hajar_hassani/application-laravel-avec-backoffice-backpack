{{--
  ./resources/views/pages/_menu.blade.php
  variables disponibles :
      - $pages Pages
 --}}


<ul class="nav navbar-nav navbar-right">
  @foreach ($pages as $page)
    @if ($page->titreMenu === 1)
      <li class="active">
        <a href="index.html">
          {{ $page->titreMenu }}
        </a>
      </li>
    @endif
      <li>
        <a href="{{ URL::route('pages.show', [
          'page' => $page->id,
          'slug' => Str::slug($page->titreMenu)
          ]) }}">
          {{ $page->titreMenu }}
        </a>
      </li>
  @endforeach
</ul>
