{{--
  ./resources/views/projets/similarProjet.blade.php
  variables disponibles :
      - $similarProjets as projet
 --}}

@foreach ($similarProjets as $projet)
  <div class="col-md-3 col-sm-6">
    <figure>
      <img src="{{ asset('img/portfolio/'.$projet->image) }}" alt="img04">
      <figcaption>
        <h3>{{ $projet->titre }}</h3>
        <span>{{ $projet->client }}</span>
        <a href="{{ URL::route('projets.show', [
              'projet' => $projet->id,
              'slug'     => Str::slug($projet->titre)
            ]) }}">
          Take a look
        </a>
      </figcaption>
    </figure>
   </div>
@endforeach
