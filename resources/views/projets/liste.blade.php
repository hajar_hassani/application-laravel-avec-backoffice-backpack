{{--
  ./resources/views/projets/liste.blade.php
  Description: ...
  vairables disponibles :
    - $projets : ARRAY(OBJ(id, titre, sousTitre, description, client))
 --}}

<ul class="grid cs-style-2">
 @foreach ($projets as $projet)
   <div class="col-md-4 col-sm-6">
     <figure>
       <img src="{{ asset('img/portfolio/'.$projet->image) }}" alt="{{ $projet->titre }}">
       <figcaption>
         <h3>{{ $projet->titre }}</h3>
         <span>{{ $projet->client}}</span>
         <a href="{{ URL::route('projets.show', [
               'projet' => $projet->id,
               'slug'     => Str::slug($projet->titre)
             ]) }}">Take a look</a>
       </figcaption>
     </figure>
   </div>
 @endforeach
 </ul>
