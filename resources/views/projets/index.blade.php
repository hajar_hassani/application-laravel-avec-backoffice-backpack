{{--
  ./resources/views/projets/index.blade.php
  Liste des créatures
  vairables disponibles :
    - $projets : ARRAY(OBJ(id, titre, sousTitre, description, client))
 --}}


   <div class="row" id="liste-projets">
       @include('projets.liste')
   </div>
