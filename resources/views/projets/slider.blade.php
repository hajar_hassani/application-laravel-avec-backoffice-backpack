{{--
  ./resources/views/projets/slider.blade.php
  Liste des créatures
  vairables disponibles :
      - $projets
 --}}

 <section id="main-slider" class="no-margin">
     <div class="carousel slide">
         <ol class="carousel-indicators">
             <li data-target="#main-slider" data-slide-to="0" class="active"></li>
             <li data-target="#main-slider" data-slide-to="1"></li>
             <li data-target="#main-slider" data-slide-to="2"></li>
         </ol>
         <div class="carousel-inner">
           @foreach ($sliders as $slide)
             <div class="item @if($loop->first) active @endif" style="background-image: url({{ asset('img/portfolio/'.$slide->image)}})">
                 <div class="container">
                     <div class="row">
                         <div class="col-sm-12">
                             <div class="carousel-content centered">
                                 <h2 class="animation animated-item-1">{{ $slide->titre}}</h2>
                                 <p class="animation animated-item-2">
                                     {!! html_entity_decode( $slide->description ) !!}
                                 </p>
                             </div>
                         </div>
                     </div>
                 </div>
             </div><!--/.item-->
            @endforeach
         </div><!--/.carousel-inner-->
     </div><!--/.carousel-->
     <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
         <i class="icon-angle-left"></i>
     </a>
     <a class="next hidden-xs" href="#main-slider" data-slide="next">
         <i class="icon-angle-right"></i>
     </a>
 </section><!--/#main-slider-->
