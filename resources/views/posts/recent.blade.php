{{--
  ./resources/views/posts/latest.blade.php
  variables disponibles :
      - $posts Post
 --}}

<h4>Recent Posts</h4>
 <ul class="recent-posts">
   @foreach ($posts as $post)
     <li>
       <a href="{{ URL::route('posts.show', [
           'post' => $post->id,
           'slug'     => Str::slug($post->titre)
         ]) }}">
         {{ $post->titre }}
       </a>
     </li>
   @endforeach
 </ul>
