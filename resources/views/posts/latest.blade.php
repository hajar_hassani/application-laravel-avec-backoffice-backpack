{{--
  ./resources/views/posts/latest.blade.php
  variables disponibles :
      - $posts Post
 --}}

<h2>Latest Blog Posts</h2>
@foreach ($posts as $post)
  <div class="row">
    <div class="col-xs-4"><a href="blog-post.html"><img src="{{ asset('img/blog/'.$post->image)}}" alt="Post Title"></a></div>
    <div class="col-xs-8">
      <div class="caption"><a href="blog-post.html">{{ $post->tire }}</a></div>
      <div class="date">{{ \Carbon\Carbon::parse ($post->created_at)->format('d M,Y') }}</div>
      <div class="intro">{{ $post->texte}}
        <a href="{{ URL::route('posts.show', [
            'post' => $post->id,
            'slug'     => Str::slug($post->titre)
          ]) }}">
          Read more...
        </a>
      </div>
    </div>
  </div>
@endforeach
