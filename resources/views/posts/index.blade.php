{{--
  ./resources/views/posts/index.blade.php
  Liste des créatures
  vairables disponibles :
      - $posts
 --}}

 <!-- Les posts -->
 <div class="section">
   <div class="container">
     <div class="row">

       @foreach ($posts as $post)
       <!-- Blog Post Excerpt -->
       <div class="col-sm-6">
         <div class="blog-post blog-single-post">
           <div class="single-post-title">
             <h2>{{ $post->titre }}</h2>
           </div>

           <div class="single-post-image">
             <img src="{{ asset('img/blog/'.$post->image) }}" alt="Post Title">
           </div>

           <div class="single-post-info">
             <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse ($post->created_at)->format('d M,Y')}}<a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
           </div>

           <div class="single-post-content">
             <p>
               {!! html_entity_decode( $post->texte ) !!}
             </p>
           <a href="{{ URL::route('posts.show', [
                 'post' => $post->id,
                 'slug'     => Str::slug($post->titre)
               ]) }}" class="btn">
               Read more
             </a>
           </div>
         </div>
       </div>
       <!-- End Blog Post Excerpt -->
       @endforeach

       <!-- Pagination -->
       <div>
         <ul class="pagination pagination-sm">
           <li>{!! $posts->links(); !!}</li>
         </ul>
       </div>
       <!-- Fin de pagination -->

     </div>
   </div>
 </div>
