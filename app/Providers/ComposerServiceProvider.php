<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

          View::composer('pages.menu', function($view){
            $view->with('pages', App\Models\Page::all());
          });

          View::composer('categories._menu', function( $view){
            $view->with('categories', \App\Models\Categorie::all());
          });
    }
}
