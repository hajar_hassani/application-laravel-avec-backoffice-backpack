<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProjetRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProjetCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProjetCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Projet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/projet');
        $this->crud->setEntityNameStrings('projet', 'projets');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation(){

        $this->crud->setValidation(ProjetRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
        // n-n relationship / montre les tags d'un projet
        $this->crud->addField([
          'type' => 'select2_multiple',
          'name' => 'tags', // the relationship name in your Model
          'entity' => 'tags', // the relationship name in your Model
          'attribute' => 'nom', // attribute on Article that is shown to admin
          'pivot' => true, // on create&update, do you need to add/delete pivot table entries?
          'options'   => (function ($query) {
              return $query->orderBy('nom', 'ASC')->get();
           }),
        ]);

        //Upload d'images des projets
       $this->crud->removeField('image');
       $this->crud->addField([
        'label' => "Image",
        'name' => "image",
        'type' => 'image',
        'upload' => true,
        'crop' => false, // set to true to allow cropping, false to disable
        'aspect_ratio' => 1,
        'prefix' => 'img/portfolio/',
      ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
