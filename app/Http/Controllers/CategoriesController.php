<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Models\Categorie;
use App\Models\Post;

class CategoriesController extends Controller {

  /**
       * Affiche le detail d'une créature
       *@param Categorie $categorie
       *@return vue categories.show
       */

    public function show(Categorie $categorie) {
      $posts = $categorie->posts()->orderBy('created_at', 'asc')->paginate(4);
      return View::make('categories.show', compact('categorie', 'posts'));

    }
}
