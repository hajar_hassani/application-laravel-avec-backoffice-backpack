<?php
namespace App\Http\Controllers;

  use Illuminate\Support\Facades\View;
  use App\Models\Projet;
  use App\Models\Tags;

  use Illuminate\Support\Facades\DB;

  // Pour récupérer des données GET ou POST
  use Illuminate\Http\Request;

class ProjetsController extends Controller {

  /**
       * Affiche le detail d'une créature
       *@param Projet $projet
       *@return vue projets.show
       */

      public function show(Projet $projet){
        $tagIds = $projet->tags->pluck('id')->toArray();
        //Récupérer les projets similaires
        $similarProjets = Projet::whereHas('tags', function ($query) use ($tagIds) {
          return $query->whereIn('id', $tagIds);
      })->where('id', '!=', $projet->id)
          ->limit(4)
          ->get();
          //->sortByDesc(function (){}$similarProjets->tags

        return View::make('projets.show',compact('projet', 'similarProjets'));
      }

    
      public function ajaxOlders(Request $request){
        $projets = Projet::orderBy('created_at', 'desc')
                  ->take(6)
                  ->offset($request->get('offset'))
                  // Pour reccupérer la variable get pour pouvoir afficher les projets suivants
                  ->get();
        return View::make('projets.liste',compact('projets'));
      }
}
