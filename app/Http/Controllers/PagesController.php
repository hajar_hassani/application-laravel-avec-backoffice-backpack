<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use App\Models\Page;
use App\Models\Projet;
use App\Models\Post;

use Illuminate\Support\Facades\DB;

class PagesController extends Controller {

  /**
   * Détails de la pages $id
   * @param integer $id [id de la page à afficher]
   *@return View       [Vue pages/show.blade.php]
  */

  public function show($id = 1){
    $page = Page::find($id);
    if ($page->id === 1):
      $projets = Projet::orderBy('created_at', 'desc')->take(6)->get();
      $sliders = Projet::where('slider', '=', 1)->take(3)->get();
      $posts = Post::orderBy('created_at', 'desc')->take(3)->get();
      return View::make('pages.show', compact('page', 'projets', 'sliders'));
    elseif ($page->id === 2):
      $projets = Projet::orderBy('created_at', 'desc')->take(6)->get();
      return View::make('pages.show', compact('page', 'projets'));
    elseif ($page->id === 3):
      $posts = Post::orderBy('created_at', 'asc')->paginate(4);
      return View::make('pages.show', compact('page', 'posts'));
    endif;
    return View::make('pages.show', compact('page'));
  }
}
