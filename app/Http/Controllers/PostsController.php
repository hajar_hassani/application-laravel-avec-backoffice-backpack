<?php
namespace App\Http\Controllers;

  use Illuminate\Support\Facades\View;
  use App\Models\Post;

  use Illuminate\Support\Facades\DB;

class PostsController extends Controller {

  /**
       * Affiche le detail d'une créature
       *@param Post $post
       *@return vue posts.show
       */

      public function show($id){
        $post = Post::find($id);
        return View::make('posts.show',compact('post'));

        $posts = orderBy('created_at', 'asc')->paginate(4);
        return View::make('posts.show', compact('posts'));

      }
}
