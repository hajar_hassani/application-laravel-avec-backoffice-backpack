/*
  ./public/js/projets/index.js
*/

$(function(){

  var offsetCalcule = 6;

  var baseUrl = $('body').attr('data-baseURL');

  $('#older-works').click(function(e){
      e.preventDefault();
      $.ajax({
        url: baseUrl + '/ajax/older-works',
        data: {
          offset: offsetCalcule
        },
        method: 'get',
        success: function(reponsePHP){
          $('#liste-projets').append(reponsePHP)
                             .find('.col-sm-6:nth-last-of-type(-n+6)');
          //offsetCalcule = offsetCalcule + 6
          offsetCalcule +=6;
        },
        error: function(){
          alert("Problème durant la transaction !");
        }
      });
  });

});
